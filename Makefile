output: main.o
	mkdir build && g++ main.o -o  build/output

main: main.cpp
	g++ -c main.cpp

run:
	./build/output

clean:
	rm -rf build/