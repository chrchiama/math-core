#pragma ide diagnostic ignored "hicpp-noexcept-move"
/* *******************************************
**
** Created by Christian Chiama on 27/03/20.
**
** @author: Christian Chiama
** @project: ∑Math™
** @module: ∑math-core
** @description: mathematical library
**
** @url: http.∑math-lang.com
** @email: christianchiama@icloud.com
**
** @version: 2.0
** @licence: MIT
**
** Copyright™ (©) 2020
**
** @filename: wegs.h
***********************************************/


#ifndef MATH_CORE_MATRIX_H
#define MATH_CORE_MATRIX_H

#include <cstdio>
#include <vector>

template<typename T,size_t N>
class Matrix {
public:
    static constexpr size_t order = N;
    using value_type = T;
    using iterator = typename std::vector<T>::iterator;
    using const_iterator = typename std::vector<T>::const_iterator;


    Matrix() = default;  // constructor
    Matrix(const Matrix &) = default;  // copy constructor
    Matrix(Matrix &&) = default;  // move constructor
    Matrix& operator = (Matrix&&) = default;
    Matrix& operator = (const Matrix&) = default;
    ~Matrix() = default;  // deconstructor

};

#endif //MATH_CORE_MATRIX_H

